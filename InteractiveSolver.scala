import scala.language.implicitConversions
import scala.annotation.tailrec

import Arithmetic._
import Constraints._

object InteractiveSolver {
  
  val skip = """skip""".r
  val definition = """definition""".r
  val splitr = """split (\d+) (\d+) (\d+)""".r
  val subst = """subst (\d+) (\d+) (\w+)""".r
  val substone = """subst (\d+)""".r
  val substitutions = """substitutions""".r
  val substprefix = """substprefix""".r
  val substprefixonce = """substprefixonce""".r
  val prefixcase = """prefix (\d+)""".r
  val suffixcase = """suffix (\d+)""".r
  val arith = """arith""".r
  val arithone = """arith (\d+)""".r
  val core = """core""".r
  val constantsplit = """constants""".r
  
//   implicit pbToDescPb(pb: Problem): DescribedProblem = (pb, List())
  
  def prefixanalysis(p: Problem, ps: List[Problem], l: List[Problem], eqs: List[Equation], e_rep: String, suffix: Boolean) = {
    val Equation(a,b) = if (suffix) eqs(e_rep.toInt).reverse else eqs(e_rep.toInt)
    
    if (a.length > 0 && b.length > 0 && a(0).isVariable && b(0).isVariable) {
      println("Case splitting on whether " + a(0) + " is a prefix of " + b(0) + " or vice versa.")
//               val f = fresh()
      val va@Variable(_) = a(0)
      val vb@Variable(_) = b(0)
      
      if (suffix) {
        interactiveSolver(
          (p.changeAll(va, vb).addDesc(va + " and " + vb + " are equal")) ::
          (p.changeAll(va, (vb ++ Word("0") ++ va).reverse).addDesc("0" + vb + " is a suffix of " + va)) ::
          (p.changeAll(va, (vb ++ Word("1") ++ va).reverse).addDesc("1" + vb + " is a suffix of " + va)) ::
          (p.changeAll(vb, (va ++ Word("0") ++ vb).reverse).addDesc("0" + va + " is a suffix of " + vb)) ::
          (p.changeAll(vb, (va ++ Word("1") ++ vb).reverse).addDesc("1" + va + " is a suffix of " + vb)) ::
          ps
        )
      } else  {
        interactiveSolver(
          (p.changeAll(va, vb).addDesc(va + " and " + vb + " are equal")) ::
          (p.changeAll(va, vb ++ Word("0") ++ va).addDesc(vb + "0 is a prefix of " + va)) ::
          (p.changeAll(va, vb ++ Word("1") ++ va).addDesc(vb + "1 is a prefix of " + va)) ::
          (p.changeAll(vb, va ++ Word("0") ++ vb).addDesc(va + "0 is a prefix of " + vb)) ::
          (p.changeAll(vb, va ++ Word("1") ++ vb).addDesc(va + "1 is a prefix of " + vb)) ::
          ps
        )
      }
    }
    else {
      if (suffix)
        println("This equation doesn't end with a variable on both sides.")
      else
        println("This equation doesn't start with a variable on both sides.")
      interactiveSolver(l)
    }
  }
  
  def interactiveSolver(l: List[Problem]): Unit = l match {
    case Nil => println("QED")
    case pt :: ps =>
    
      
//       val depth = desc.length
    
      println("\n\n")
      if (ps.isEmpty)
        println("1 problem to solve")
      else
        println(l.size + " problems to solve")
        
      println("\nDescription")
      println(pt.desc)
      println("End Description\n")
    
//       println("Original problem")
// //       
//       println(pt.eqs.mkString("\n") + "\n")
// //       
//       println(pt.simplify.eqs.mkString("\n") + "\n")
// //       
//       println(pt.simplify.removeDefinitions.eqs.mkString("\n") + "\n")
    
      val p = pt.cleanup
      val eqs = p.eqs.toList.sortBy(_.toString).sortBy(_.toString.size)
      
      println(eqs.zipWithIndex.map { case (eq,i) => i + ":\t" + eq }.mkString("\n"))
      
      if (p.isContradictory) {
        println("These equations are contradictory, NEXT.")
        interactiveSolver(ps)
      }
      else {
      
        println("\nWhat should I do?")
        val t = scala.io.StdIn.readLine()
        
        t match {
          case skip() => 
            println("Skipping this problem, it is too difficult.")
            interactiveSolver(ps)
          case splitr(rep_eq,rep_side,rep_i) =>
            val eq = rep_eq.toInt
            val side = rep_side.toInt
            val i  = rep_i.toInt 
            
            println("Splitting equation " + (eq,side) + " based on constant number " + i)
            
  //           val w = eqs(eq)(side).vars.filter(_.isWord)(i)
            
            val splitter = eqs(eq)(side)
            val (w,j) = splitter.vars.zipWithIndex.filter(_._1.isWord)(i)
            
            w match {
              case Word(u) if (u.size != 1) => throw new Exception("invalid split")
              case Word(u) => 
              
                val a = eqs(eq)(1 - side)
                interactiveSolver(a.vars.zipWithIndex.flatMap {
                  case (Word(v), k) =>
                    if (u == v) {
                      val e1 = a.take(k) <-> splitter.take(j)
                      val e2 = a.drop(k+1) <-> splitter.take(j+1)
                      
                      Some((p ++ Set(e1,e2)).addDesc("Match on constant " + u))
                    }
                    else if (v.contains(u)) {
                      throw new Exception("Not implemented")
                    }
                    else 
                      None
                  case (Variable(x), k) =>
                    val f1 = Variable(x + "1")
                    val f2 = Variable(x + "2")
                    val replacement = f1 ++ w ++ f2
                    val e1 = a.take(k) ++ f1 <-> splitter.take(j)
                    val e2 = f2 ++ a.drop(k+1) <-> splitter.drop(j+1)
  //                   println("replacing " + x + " with: " + replacement)
                    Some((p ++ Set(e1,e2)).changeAll(Variable(x), replacement).addDesc (Variable(x) + " <- " + replacement))
                    
                } ::: ps)
                
              case _ => throw new Exception("not possible because of previous filter")
            }
            
          case definition() => 
            interactiveSolver(p.removeDefinitions :: ps)
            
          case subst(e1_rep, e2_rep, dir) if (dir == "ltr" || dir == "rtl") =>
            val Equation(a,b) = eqs(e1_rep.toInt)
            val e2 = eqs(e2_rep.toInt)
            
            val sub = if (dir == "ltr") Equation(a,b) else Equation(b,a)
            
            val toadd = e2.substitutions(sub)
//             println("Adding equations")
//             println(toadd)
            interactiveSolver((p ++ toadd) :: ps)
            
          case substone(e_rep) =>
            val e@Equation(a,b) = eqs(e_rep.toInt)
//             val e2 = eqs(e2_rep.toInt)
            
//             val sub = if (dir == "ltr") Equation(a,b) else Equation(b,a)
            
            val toadd = eqs.flatMap(f => f.substitutions(e) ++ f.substitutions(Equation(b,a)))
//             println("Adding equations")
//             println(toadd)
            interactiveSolver((p ++ toadd.toSet) :: ps)
            
          case substitutions() =>
            p.closeSubstitutions match {
              case None =>
                println("There is a contradiction here")
                interactiveSolver(ps)
              case Some(newp) =>
                interactiveSolver(newp.findCoreEquations :: ps)
            }
            
          case substprefix() =>
            val newp = p.closeSubstPrefix
            
//             println("Checking for contradiction")
//             if (newp.isContradictory) {
//               println("There is a contradiction here")
//               interactiveSolver(ps)
//             }
//             else {
              interactiveSolver(newp.findCoreEquations :: ps)
//             }
            
          case substprefixonce() =>
            val newp = p.substitutions.explorePrefixes
            
            interactiveSolver(newp.findCoreEquations :: ps)
          
            
          case prefixcase(e_rep) =>
            prefixanalysis(p, ps, l, eqs, e_rep, false)
            
          case suffixcase(e_rep) =>
            prefixanalysis(p, ps, l, eqs, e_rep, true)
            
            
          case core() =>
            interactiveSolver((p.findCoreEquations) :: ps)
            
          case arith() =>
            interactiveSolver((p.exploreArithmeticPrefix) :: ps)
            
          case arithone(e_rep) =>
            val e = eqs(e_rep.toInt)
            interactiveSolver((p.exploreArithmeticPrefix(e)) :: ps)
          
          case constantsplit() =>
            interactiveSolver(p.findConstants ::: ps)
          
          case _ =>
            println("Did not understand the command")
            interactiveSolver(l)
            
  //           println(w)
            
        }
      }
      
//       if (t == "skip") {
//         val q = p
//         interactiveSolver(ps)
//       } else if (
    
  }
/*  
  def deuxetats(): Problem = {    
  
    val a = Variable("a")
    val b = Variable("b")
    val c = Variable("c")
    val d = Variable("d")
    val e = Variable("e")
    val f = Variable("f")
    val g = Variable("g")
    val h = Variable("h")
    val x = Variable("x")
    val y = Variable("y")
    val r = Variable("r")
    val s1 = Variable("s1")
    val s2 = Variable("s2")
    
    
    val e0 = a ++ x ++ b <-> e ++ y ++ f
    val e1 = a ++ c ++ x ++ d ++ b <-> e ++ g ++ y ++ h ++ f
    val e2 = a ++ c ++ c ++ x ++ d ++ d ++ b <-> e ++ g ++ g ++ y ++ h ++ h ++ f
    
    val t1 = a ++ c ++ c ++ c ++ x ++ d ++ d ++ d ++ b <-> r ++ Word("0") ++ s1
    val t2 = a ++ c ++ c ++ c ++ x ++ d ++ d ++ d ++ b <-> r ++ Word("1") ++ s1
    
    val Problem(Set(
      e0,e1,e2,t1,t2
    ), Description(List()))
  }*/
  
  def plandowski(): Problem = {
  
    val r = Variable("r")
    val s1 = Variable("s1")
    val s2 = Variable("s2")
  
    def l(k: Int): Set[Concatenation] = {
      if (k == 1)
        Set(Variable("a1"), Variable("b1"))
      else {
        val r = l(k-1)
        r.flatMap { s => Set(
          Variable("a" + k) ++ s ++ Variable("ap" + k),
          Variable("b" + k) ++ s ++ Variable("bp" + k)
        )}
      }
    }
    
//     println(l(4).toList.map(_.toString).sorted)
//     println(l(4).size)
//     
//     for (c <- l(4)) {
//       println(c)
//       println(c.renameVars(f))
//       println(c.renameVars(g))
//       println("===")
//     }
    
    def f(s: String) = s.replace("bp", "D").replace("ap","B").replace("b","C").replace("a","A")
    def g(s: String) = s.replace("bp", "H").replace("ap","F").replace("b","G").replace("a","E")
    
    val cs = l(4).toList.sortBy(_.toString).sortBy(_.toString.size)
    val eqs = cs.take(15).map { c =>
      c.renameVars(f) <-> c.renameVars(g)
    } ::: List(
      cs(15).renameVars(f) <-> r ++ Word("0") ++ s1,
      cs(15).renameVars(g) <-> r ++ Word("1") ++ s2
    )
    
    Problem(eqs.toSet, Description(List()))
//     val cs = Array(c0,c1,c2,c3,c4,c5,c6,c7,c8,c9,c10,c11,c12,c13,c14,c15)
    
    
    
  }
  
  
  def main(args: Array[String]) = {
    val A = Variable("A")
    val B = Variable("B")
    val C = Variable("C")
    val D = Variable("D")
    val E = Variable("E")
    val F = Variable("F")
    val G = Variable("G")
    val R = Variable("R")
    val S1 = Variable("S1")
    val S2 = Variable("S2")
    
    val t1 = A ++ D ++ B ++ D ++ C
    val T1 = E ++ D ++ F ++ D ++ G
    
    val tleft = A ++ t1 ++ B ++ D ++ C
    val Tleft = E ++ T1 ++ F ++ D ++ G
    
    val trightleft = A ++ D ++ B ++ tleft ++ C
    val Trightleft = E ++ D ++ F ++ Tleft ++ G
    
    val trightrightleft = A ++ D ++ B ++ trightleft ++ C
    val Trightrightleft = E ++ D ++ F ++ Trightleft ++ G
    
    val k1 = A ++ D ++ B ++ E ++ D ++ F
    val k2 = E ++ D ++ F ++ A ++ D ++ B
    
    
    
    val e1 = tleft <-> Tleft
    val e2 = trightleft <-> Trightleft
    val e3 = k1 <-> R ++ Word("0") ++ S1
    val e4 = k2 <-> R ++ Word("1") ++ S2
    
//     val a1 = Equation("XE XD XF XA XA XD XB XD XC XB XD XG = XA XD XB XA XA XD XB XD XC XB XD XC")
//     val a2 = Equation("XE XD XF XE XE XD XF XD XG XF XD XG = XA XD XB XE XE XD XF XD XG XF XD XC")
    

    
  
    val lrlrrlpb = Problem(Set(e1,e2,e3,e4), Description(List()))

    
    val problem = Problem("""
      XA XC XB  = XE XC XF
      XA XC XC XB  = XE XC XC XF
      XA XC XC XC XB  = XR '0' XS1
      XE XC XC XC XF  = XR '1' XS2
    """)
    
    
//     val problem = deuxetats()
//     val problem = plandowski()
//     val problem = plandowski()
//      val problem = Problem("""
//       XA XX XB  = XE XX XF 
//       XC XX XD  = XG XX XH 
//       XA XA XX XB XB  = XE XE XX XF XF 
//       XA XC XX XD XB  = XE XG XX XH XF 
//       XC XA XX XB XD  = XG XE XX XF XH 
//       XC XC XX XD XD  = XG XG XX XH XH 
//       XA XA XA XX XB XB XB  = XE XE XE XX XF XF XF 
//       XA XA XC XX XD XB XB  = XE XE XG XX XH XF XF 
//       XA XC XA XX XB XD XB  = XE XG XE XX XF XH XF 
//       XA XC XC XX XD XD XB  = XE XG XG XX XH XH XF 
//       XC XA XA XX XB XB XD  = XG XE XE XX XF XF XH 
//       XC XA XC XX XD XB XD  = XG XE XG XX XH XF XH 
//       XC XC XA XX XB XD XD  = XG XG XE XX XF XH XH 
//       XC XC XC XX XD XD XD  = XG XG XG XX XH XH XH
//       XA XC XA XC XX XD XB XD XB = XR '0' XS1
//       XE XG XE XG XX XH XF XH XF = XR '1' XS2
//     """)

//       val problem = Problem("""
//         XE XX XF = XK XX XL
//         XA XX XB = XG XX XH
//         XC XX XD = XI XX XJ
//         XE XE XX XF XF = XK XK XX XL XL
//         XE XA XX XB XF = XK XG XX XH XL                                                                                                                                             
//         XE XC XX XD XF = XK XI XX XJ XL                                                                                                                                             
//         XA XE XX XF XB = XG XK XX XL XH
//         XA XA XX XB XB = XG XG XX XH XH
//         XA XC XX XD XB = XG XI XX XJ XH
//         XC XE XX XF XD = XI XK XX XL XJ
//         XC XA XX XB XD = XI XG XX XH XJ
//         XC XC XX XD XD = XI XI XX XJ XJ
//         XE XE XE XX XF XF XF = XK XK XK XX XL XL XL
//         XE XE XA XX XB XF XF = XK XK XG XX XH XL XL
//         XE XE XC XX XD XF XF = XK XK XI XX XJ XL XL
//         XE XA XE XX XF XB XF = XK XG XK XX XL XH XL
//         XE XA XA XX XB XB XF = XK XG XG XX XH XH XL
//         XE XA XC XX XD XB XF = XK XG XI XX XJ XH XL
//         XE XC XE XX XF XD XF = XK XI XK XX XL XJ XL
//         XE XC XA XX XB XD XF = XK XI XG XX XH XJ XL
//         XE XC XC XX XD XD XF = XK XI XI XX XJ XJ XL
//         XA XE XE XX XF XF XB = XG XK XK XX XL XL XH
//         XA XE XA XX XB XF XB = XG XK XG XX XH XL XH
//         XA XE XC XX XD XF XB = XG XK XI XX XJ XL XH
//         XA XA XE XX XF XB XB = XG XG XK XX XL XH XH
//         XA XA XA XX XB XB XB = XG XG XG XX XH XH XH
//         XA XA XC XX XD XB XB = XG XG XI XX XJ XH XH
//         XA XC XE XX XF XD XB = XG XI XK XX XL XJ XH
//         XA XC XA XX XB XD XB = XG XI XG XX XH XJ XH
//         XA XC XC XX XD XD XB = XG XI XI XX XJ XJ XH
//         XC XE XE XX XF XF XD = XI XK XK XX XL XL XJ
//         XC XE XA XX XB XF XD = XI XK XG XX XH XL XJ
//         XC XE XC XX XD XF XD = XI XK XI XX XJ XL XJ
//         XC XA XE XX XF XB XD = XI XG XK XX XL XH XJ
//         XC XA XA XX XB XB XD = XI XG XG XX XH XH XJ
//         XC XA XC XX XD XB XD = XI XG XI XX XJ XH XJ
//         XC XC XE XX XF XD XD = XI XI XK XX XL XJ XJ
//         XC XC XA XX XB XD XD = XI XI XG XX XH XJ XJ
//         XC XC XC XX XD XD XD = XI XI XI XX XJ XJ XJ
//         XA XC XE XA XX XB XF XD XB = XR '0' XS1
//         XG XI XK XG XX XH XL XJ XH = XR '1' XS2
//       """)
    
    

//      val problem = Problem("""
//       XA XX XB  = XE XX XF 
//       XC XX XD  = XG XX XH 
//       XA XA XX XB XB  = XE XE XX XF XF 
//       XA XC XX XD XB  = XE XG XX XH XF 
//       XC XA XX XB XD  = XG XE XX XF XH 
//       XC XC XX XD XD  = XG XG XX XH XH 
//       XA XC XA XX XB XD XB = XR '0' XS1
//       XE XG XE XX XF XH XF = XR '1' XS2
//     """)
    
    
//       val c = Concatenation("XR '1' XS2")
//       val a = Concatenation("XR '1' XS2")
//       val b = Concatenation("XE XG XE XG XX XH XF XH XF")
      
//       println(c.substitutions(a,b))
    interactiveSolver(List(problem))
    
//     val h = Equation("XE XE XD XF XD XF XD = XA XA XD XB XD XC '0' XB XD XC '0'")
    
//     println(h.findConstants)
    
//     val h = Problem("""
//       '0' = 
//       = XR '0' XS1
//       = XR '1' XS2
//       """
//     )
    
//     println(h + "\n")
    
//     val q = h.explorePrefixes
    
//     println("q")
//     println(q)
    
    
//     println(problem.eqs.mkString("\n"))
//     
//     println((problem.getFormula + Equality(G.msize, C.msize) + NonEquality((A++B).msize, (E++F).msize)).isSatisfiable)
    
//     val Equation(k,l) = Equation(" = '0'")
//     val e = Equation(k,l)

//     val r1 = Equation("XF29 '1' XF30 XF XE XF29 '0' XF2 XE XF29 '1' XF30 XF XE XF29 '0' XF2 XF29 '1' XF30 XB XF29 '1' XF30 XC XB XF29 '1' XF30 XC = XE XF29 '1' XF30 XF XF29 '1' XF30 XG XF XF29 '1' XF30 XG")
//     
//     val r2 = Equation("XF29 '0' XF2 XF29 '1' XF30 XB XE XF29 '1' XF30 XF XE XF29 '0' XF2 XE XF29 '1' XF30 XF XE XF29 '0' XF2 XF29 '1' XF30 XB XF29 '1' XF30 XC XB XF29 '1' XF30 XC XC = XE XF29 '1' XF30 XF XF29 '1' XF30 XG XF XF29 '1' XF30 XG XG")
//     
//     println(r1)
//     println(r2)
//     val r1 = Equation("XD XF XE XD = XE XD XF XD")
    
//     println(r1.simplify)

    
//     val pr = Problem(Set(r1,r2))
    
//     println(pr.divergent)

//     val f = 

    
    
//     println(e)
//     println(e.simplify)
//     println(e.isContradictory)
//     println(k.isEmpty)
//     println(l)
//     println(l.containsWord())
    
//     solver(problem, true)
  }
}

