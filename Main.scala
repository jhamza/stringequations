import scala.language.implicitConversions
import scala.annotation.tailrec

object StringEquations {


//   val target = Equation("dbafdB = fdBadb")
//   val target = Equation("badB = Badb")
  var target = Equation("adbAdB = AdBadb")

  case class Variable(s: String) {
    override def toString() = s
    
    def compare(s2: String) = s compare s2
  
  }
  
  implicit def ordering[A <: Variable]: Ordering[Variable] = Ordering.by(_.toString)
  
  case class Concatenation(vars: List[Variable]) {
    
    
    def <->(c: Concatenation) = Equation(this,c)
    
    def ++(c: Concatenation) = Concatenation(vars ++ c.vars)
  
    def take(i: Int) = Concatenation(vars.take(i))
    def drop(i: Int) = Concatenation(vars.drop(i))
  
    def sliding(i: Int) = vars.sliding(i).map(Concatenation(_))
    def length = vars.length
    def size = vars.length
    def sorted = vars.sorted
 
    def reverse = Concatenation(vars.reverse)
    
    override def toString() = vars.mkString
  }
  
  object Concatenation {
  
    def apply(): Concatenation = Concatenation(List())
  
  }
  
  case class ExplConcatenation(c: Concatenation, prevC: Option[Concatenation], e: Option[Equation]) {
    override def equals(o: Any) = o match {
      case ExplConcatenation(c2, _, _) => c == c2
      case _ => false
    }
    
    override def hashCode = c.hashCode
    
    override def toString() = c + " is " + prevC + " with substitution " + e
  }
  
  implicit def ConcatenationToExplConcatenation(c: Concatenation) = ExplConcatenation(c,None,None)
  
  implicit def variableToConcatenation(v: Variable): Concatenation = Concatenation(List(v))
  implicit def variablesToConcatenation(vars: List[Variable]): Concatenation = Concatenation(vars)
  
  
  
  case class Equation(a: Concatenation, b: Concatenation) {
    def trivial = a == b
  
    def reverse = a.reverse <-> b.reverse
  
    override def equals(o: Any) = o match {
      case Equation(c,d) => (a == c && b == d) || (a == d && b == c)
      case _ => false
    }
    
    override def hashCode = a.hashCode * b.hashCode
    
    override def toString() = a + " = " + b
  }
  
  object Equation {
  
    def apply(s: String): Equation = {
      val l = s.split("=")
      val a = l(0)
      val b = l(1)
      Concatenation(a.replace(" ","").toList.map(c => Variable(c.toString))) <->
      Concatenation(b.replace(" ","").toList.map(c => Variable(c.toString)))
    }
    
  }
 
  
  
  def modify(i: Int, a: Concatenation, b: Concatenation, c: Concatenation): Concatenation = {
    c.take(i) ++ b ++ c.drop(i + a.length)
  }
  
  def modify(s: Set[Int], a: Concatenation, b: Concatenation, c: Concatenation): Concatenation = {
    s.toList.sorted.reverse.foldLeft(c)((d,i) => modify(i,a,b,d))
  }
  
  def changeAll(a: Concatenation, b: Concatenation, c: Concatenation): Concatenation = {
    val va = a.vars
    val vb = b.vars
    val vc = c.vars 
    val na = va.length
    
    def loop(l: List[Variable]): List[Variable] = {
      if (l.length < na) l
      else if (l.take(na) == va) vb ::: loop(l.drop(na))
      else l.head :: loop(l.tail)
    }
    
    Concatenation(loop(vc))
  }
  
  def changeAll(e: Equation, c: Concatenation): Concatenation = {
    val Equation(a,b) = e
    changeAll(a,b,c)
  }
  
  def changeAll(e: Equation, f: Equation): Equation = {
    val Equation(a,b) = e
    val Equation(c,d) = f
    Equation(changeAll(a,b,c), changeAll(a,b,d))
  }
  
  def changeAll(l: List[Equation], e: Equation): Equation = {
    l.foldLeft(e)((r,h) => changeAll(h,r))
  }
  
  
  def changeAll(e: Equation, s: Set[Equation]): Set[Equation] = {
    s.map(changeAll(e,_))
  }
  
  def changeAll(l: List[Equation], e: Set[Equation]): Set[Equation] = {
    l.foldLeft(e)((r,h) => changeAll(h,r))
  }
  
  
  def substitutions(a: Concatenation, b: Concatenation, c: Concatenation): Set[Concatenation] = {
    val indices = c.sliding(a.size).
      zipWithIndex.
      filter { case (l,_) => l == a }.
      map { _._2 }.toSet
    
    indices.toSet.subsets.toSet.map((s: Set[Int]) => modify(s,a,b,c))
  }
  
  def substitutions(e: Equation, c: Concatenation): Set[Concatenation] = {
    substitutions(e.a, e.b, c) ++ substitutions(e.b, e.a, c)
  }
  
  def substitutions(s: Set[Equation], c: Concatenation): Set[Concatenation] = {
//     println("subbing: " + c)
    s.flatMap(e => substitutions(e,c))
  }
  
  def substitutions(s: Set[Equation], r: Set[Concatenation]): Set[Concatenation] = {
    s.flatMap(e => r.flatMap(c => substitutions(e,c)))
  }
  
  def substitutionsWithExpl(target: Concatenation, s: Set[Equation], r: Set[ExplConcatenation]): Set[ExplConcatenation] = {
    println(r.mkString("\n"))
    println(r.size + " explained concatenations")
    println("=====")
    def loop(acc: Set[ExplConcatenation], l: List[Equation]): Set[ExplConcatenation] = {
      if (acc.contains(target)) acc
      else
      l match {
        case Nil => acc
        case e :: es => 
          def subloop(subAcc: Set[ExplConcatenation], lr: List[ExplConcatenation]): Set[ExplConcatenation] = {
            if (subAcc.contains(target)) subAcc
            else
              lr match {
              case Nil => subAcc
              case ec :: ecs => 
                val s = substitutions(e, ec.c).map(c => ExplConcatenation(c,Some(ec.c),Some(e)))
                subloop(subAcc ++ (s -- subAcc), ecs)
            }
          }
          loop(subloop(acc, r.toList), es) 
      }
    }
    loop(r, s.toList)
    
//     s.flatMap(e => r.flatMap(ec => substitutions(e,ec.c).map(c => ExplConcatenation(c,Some(ec.c),Some(e)))))
  }
  
  def substitutions(s: Set[Equation], e: Equation): Set[Equation] = {
//     println("subst: " + e)
    val Equation(a,b) = e
    
    (substitutions(s,a).map(l => Equation(l,b)) ++
    substitutions(s,b).map(l => Equation(a,l))).filter(!_.trivial)
  }
  
  
  def substitutionseq(s: Set[Equation], s2: Set[Equation]): Set[Equation] = {
    println("to substitute: " + s2.size)
    s2.flatMap(e => substitutions(s,e))
  }
  
  
  def substitutions(s: Set[Equation]): Set[Equation] = {
//     if (s.contains(target))
//       throw new Exception("FOUND IT")
    println(s.map(_.toString).toList.sorted.sortBy(_.size).mkString("\n"))
    println(s.size + " equations to substitute")
    val subbed = s.flatMap(e => substitutions(s,e))
    println("megatrimming")
    val trimmed = trim(subbed)
    println("finished megatrimming")
    val r = simplify(trimmed)
    println("finished simplify")
    r
  }
  
  @tailrec
  def fixpoint[T](x: T, f: T => T): T = {
    val y = f(x)
    if (x == y) y
    else fixpoint(y, f)
  }
  
  @tailrec
  def fixpoint2[T](x: T, f: T => T, g: T => T): T = {
    val y = f(g(x))
    if (x == y) y
    else fixpoint2(y, f, g)
  }
  
  def simplify(e: Equation): Equation = {
    val x = e.a
    val y = e.b
    
    
    def lll(a: List[Variable], b: List[Variable]): (List[Variable], List[Variable]) = (a,b) match {
      case (Nil,_) => (a,b)
      case (_,Nil) => (a,b)
      case (x :: xs, y :: ys) =>
        if (x == y) lll(xs,ys)
        else (a,b)
    }
    
    val (i,j) = lll(x.vars,y.vars)
    val (k,l) = lll(i.reverse,j.reverse)

    Equation(k.reverse,l.reverse)
//     
//     val (c,d) = loop(x.vars,y.vars)
//     val (e,f) = loop(c.reverse,d.reverse)
//     
//     Equation(e.reverse, f.reverse)
  }
  
  def simplify(s: Set[Equation]): Set[Equation] = {
    s.map(simplify)
  }
  
  def explorePrefixes(e: Equation): Set[Equation] = {
    
    @tailrec
    def loop(pa: List[Variable], pb: List[Variable], a: List[Variable], b: List[Variable], acc: Set[Equation]): Set[Equation] = {
      (a, b) match {
        case (Nil, Nil) => 
          acc
        case (x::xs,y::ys) =>
          if (pa.sorted == pb.sorted || a.sorted == b.sorted) {
//             println(pa.sorted,pb.sorted,a.sorted,b.sorted,acc)
            loop(pa :+ x, pb :+ y, xs, ys, acc ++ Set(pa <-> pb,a <-> b).filter(!_.trivial))
          }
          else
            loop(pa :+ x, pb :+ y, xs, ys, acc)
        
        case _ => acc
//         case _ => throw new Exception("Equation(a,b) with a.length != b.length")
      }
    }
    
    val Equation(a,b) = e
    val res =
      loop(List(), List(), a.vars, b.vars ,Set()) ++
      loop(List(), List(), a.vars.reverse, b.vars.reverse, Set()).map { e => e.reverse }
    res
  }
  
  
  def explorePrefixes(s: Set[Equation]): Set[Equation] = {
    println(s.map(_.toString).toList.sorted.sortBy(_.size).mkString("\n"))
    println(s.size + " equations to explore")
//     if (s.contains(target))
//       throw new Exception("FOUND IT")
    val res = s.flatMap(explorePrefixes)
//     println(res.show)
//     println("mememetrimming")
    val r = simplify(trim(res))
//     println("mims")
    r
  }
  
  def areEqual(s: Set[Equation], a: Concatenation, b: Concatenation): Boolean = {
    println(s.size + " equations")
    if (fixpoint(Set(a), (r: Set[Concatenation]) => substitutions(s,r)).contains(b)) true
    else {
      val s2 = substitutions(explorePrefixes(s))
      if (s == s2) false
      else areEqual(s2, a, b)
    }
  }
  
  
  def traceBack(s: Set[ExplConcatenation], source: Concatenation, target: Concatenation): List[ExplConcatenation] = {
    val m = s.map { case ec@ExplConcatenation(c,c2,e) => (c,ec) }.toMap
    
    def loop(tg: Concatenation, acc: List[ExplConcatenation]): List[ExplConcatenation] = {
      if (source == tg) acc.reverse
      else {
        val ec = m(tg)
        println(ec)
        if (ec.prevC.isEmpty) acc.reverse
        else loop(ec.prevC.get.c, ec :: acc)
      }
    }
    
    loop(target, List())
  }
/*  
  def findCoreEquations(s: Set[Equation]): Set[Equation] = {
    s.find { case Equation(c,d) =>
      fixpoint[Set[Concatenation]](Set(c), substitutions(s - Equation(c,d), _)).contains(d) ||
      (s-Equation(c,d)).exists { case Equation(a1,b1) =>
        (s-Equation(c,d)).exists { case Equation(a2,b2) => 
          (c == a1 ++ a2 && d == b1 ++ b2) ||
          (c == b1 ++ b2 && d == a1 ++ a2) ||
          (c == a1 ++ b2 && d == b1 ++ a2) ||
          (c == b1 ++ a2 && d == a1 ++ b2)
        }
      }
      
    } match {
      case None => s
      case Some(e) => findCoreEquations(s-e)
    }
  }*/
  
  
  def removesubst(s: Set[Equation]): Set[Equation] = {
    println("removingSubst from " + s.size + " equations")
    s.find { case Equation(c,d) =>
      fixpoint[Set[Concatenation]](Set(c), substitutions(s - Equation(c,d), _)).contains(d)
    } match {
      case None => s
      case Some(e) => s - e
    }
  }
  
  def removeconcat(s: Set[Equation]): Set[Equation] = {
    println("removing concats from " + s.size + " equations")
    
    var res = s
    for (Equation(a,b) <- s) {
      for (i <- 1 to a.length-2) {
        for (j <- 1 to b.length-2) {
          val a1 = a.take(i)
          val a2 = a.drop(i)
          val b1 = b.take(j)
          val b2 = b.drop(j)
          
          if (            (s.contains(a1 <-> b1) && s.contains(a2 <-> b2)))
            res = res - Equation(a,b)
  
        }
      }
    }
    
    res
  }
  
  
//   def findCoreEquations(s: Set[Equation]): Set[Equation] = {
//     println("trimming: " + s.size + " equations")
//    
//     var res = s
//    
//     for (Equation(a1,b1) <- s) {
//       for (Equation(a2,b2) <- s) {
//         val e1 = Equation(a1 ++ a2, b1 ++ b2)
//         val e2 = Equation(a1 ++ b2, b1 ++ a2)
//         res = res - e1
//         res = res - e2
//       }
//     }
//     
//     if (res.size == s.size) s
//     else findCoreEquations(res)
//   }
  
  def trim(s: Set[Equation]): Set[Equation] = {
    fixpoint(s, removeconcat)
  }
  
  def findCoreEquations(s: Set[Equation]): Set[Equation] = {
    fixpoint2(s, removeconcat, removesubst)
  }
  
  def main(args: Array[String]) = {

    val A = Variable("A")
    val B = Variable("B")
    val C = Variable("C")
    val D = Variable("D")
    val E = Variable("E")
    val F = Variable("F")
    val G = Variable("G")
    val R = Variable("R")
    val S1 = Variable("S1")
    val S2 = Variable("S2")
    
    val t1 = A ++ D ++ B ++ D ++ C
    val T1 = E ++ D ++ F ++ D ++ G
    
    val tleft = A ++ t1 ++ B ++ D ++ C
    val Tleft = E ++ T1 ++ F ++ D ++ G
    
    val trightleft = A ++ D ++ B ++ tleft ++ C
    val Trightleft = E ++ D ++ F ++ Tleft ++ G
    
    val trightrightleft = A ++ D ++ B ++ trightleft ++ C
    val Trightrightleft = E ++ D ++ F ++ Trightleft ++ G
    
//     val k1 = A ++ D ++ B ++ E ++ D ++ F
//     val k2 = E ++ D ++ F ++ A ++ D ++ F
    
    
    val e0 = t1 <-> T1
    val e1 = tleft <-> Tleft
    val e2 = trightleft <-> Trightleft
//     val e3 = k1 <-> R ++ Word("0") ++ S1
//     val e4 = k2 <-> R ++ Word("1") ++ S2
    
    
    
//     val a1 = Equation("EDFAADBDCB = AdBaadbdcb")
//     val a2 = Equation("adbaadbdcbdc = AdBaadbdcbdC")
//     val a3 = Equation("adbAAdBdCBdc = AdBAAdBdCBdC")
    
//     val def1 = c <-> e ++ C
//     val def2 = A <-> a ++ f
//     val def3 = B <-> g ++ b
//     val def4 = e <-> h ++ d
//     val defs = List(def1,def2,def3,def4)
    val defs = List()
    
    target = simplify(changeAll(defs, target))
    
    println("target is: " + target)
    
    val p = Set(e1,e2)
//     val p = Set(e0,e1,e2)
//     val pp = changeAll(def1, p)
//     val ppp = changeAll(def2, pp)
    val q = changeAll(defs, p)
//     val q = ppp
    
    println("Simplified problem")
    println(q)
    
    println("simplify")
    println(simplify(Equation("adbaadbdeCbdeC = AdBaadbdeCbdC")))
    
    val ee = fixpoint2[Set[Equation]](q, substitutions, explorePrefixes)
    
    println("Computed " + ee.size + " equations")
    println(ee.mkString("\n"))
    println("==============")
    println("Core Equations")
    println(findCoreEquations(ee).toList.map(_.toString).sorted.sortBy(_.size).mkString("\n"))
    println("Core Equations ^")
    println("==============")

    
    
//     val r = Set[ExplConcatenation](ExplConcatenation(trightrightleft, None, None))
    
//     val goal = simplify(changeAll(defs,e3))
//     
//     val Equation(u,v) = goal
//     
//     val allSubsts = fixpoint[Set[Equation]](Set(goal), substitutionseq(ee,_))
//     
//     println("Computed reachable equivalent from: " + goal)
//     println(allSubsts.mkString("\n"))
//     println("=======\n\n")
//     
// //     println(allSubsts.contains(v))
//     
//     var i = 0
//     val n = allSubsts.size
//     for (e <- allSubsts) {
//       i += 1
//       println("visiting: " + i + " / " + n)
//       val pre = fixpoint[Set[Equation]](Set(e), explorePrefixes)
//       val r = findCoreEquations(pre)
//       val rr = r.filterNot { case Equation(a,b) => 
//         fixpoint[Set[Concatenation]](Set(a), substitutions(ee,_)).contains(b)
//       }
// //       println(pre)
// 
//       println("The core")
//       println(r)
//       println("The remaining core")
//       println(rr)
// //       if (r.forall { case Equation(a,b) => 
// //         fixpoint[Set[Concatenation]](Set(a), substitutions(ee,_)).contains(b)
// //       })
//       if (rr.isEmpty)
//         throw new Exception("wowowow")
//       println("====")
//     }
    
//     val x1 = a ++ d ++ b ++ A ++ d ++ B
//     val y1 = A ++ d ++ B ++ a ++ d ++ b
//     
//     val x2 = c ++ C
//     val y2 = C ++ c
//     
//     val x3 = a ++ d ++ b ++ A ++ d ++ B ++ a ++ a ++ d ++ b ++ d ++ c ++ b ++ d ++ c ++ C ++ c
//     val y3 = A ++ d ++ B ++ a ++ d ++ b ++ a ++ a ++ d ++ b ++ d ++ c ++ b ++ d ++ C ++ c ++ c
//     
//     val f1 = x1 <-> y1
//     val f2 = x2 <-> y2
//     val f3 = x3 <-> y3
//     
//     val nn = Set(f1,f2,f3)
//     println("======= FIN =======")
//     println(findCoreEquations(nn))
//     println(fixpoint[Set[Concatenation]](Set(x3), substitutions(Set(f1,f2), _)))
//     Set(adbAdB = AdBadb, cC = Cc, adbAdBaadbdcbdcCc = AdBadbaadbdcbdCcc)
    
//     println(traceBack(allSubsts,trightrightleft,Trightrightleft))
//     println(areEqual(Set(e1,et),t3,t4)) 
//     println(areEqual(Set(e1,e2),s5,s6)) 
//     println(areEqual(Set(e1,e2,e3),s7,s8))
    
//     println(explorePrefixes(e2).show)
    
//     println(modify(1, s1, s2, s3))
//     println(modify(3, s1, s2, s3).show)
//     println(modify(Set(1,3), s1, s2, s3))
//     println(substitutions(s1, s2, s3).show)
//     println(substitutions(Set(e1,e2)).show)
//     println("===")
//     val es = fixpoint(Set(e1,e2), substitutions)
//     val es2 = fixpoint[Set[Equation]](es, explorePrefixes)
//     
//     println(es2.size)
//     val ee = fixpoint2[Set[Equation]](Set(e1,e2), substitutions, explorePrefixes)
//     val ee = fixpoint2[Set[Equation]](Set(e1,e2), explorePrefixes, substitutions)
//     println(ee.size)
    
//     println(ee -- es2)
  }

}
