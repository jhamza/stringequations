import scala.annotation.tailrec

import Arithmetic._

import scala.language.implicitConversions

object Constraints {
  @tailrec
  def fixpoint[T](x: T, f: T => T): T = {
    val y = f(x)
    if (x == y) y
    else fixpoint(y, f)
  }
  
  @tailrec
  def fixpoint2[T](x: T, f: T => T, g: T => T): T = {
    val y = f(g(x))
    if (x == y) y
    else fixpoint2(y, f, g)
  }
  
  def prefixes[T](l: List[T]): List[List[T]] = l match {
    case Nil => List(Nil)
    case x :: xs => Nil :: prefixes(xs).map { x :: _ }
  }
  
  def mult(x: Variable, i: Int): String = "* " + i + " " + x

  
  case class Description(desc: List[String]) {
    def add (s: String) = Description(desc :+ s)
    
    override def toString: String = {
      desc.zipWithIndex.map { case (s,i) => ("sub" * i) + "case: " + s }.mkString("\n")
    }
    
  }
  
  object Description {
    def apply(): Description = Description(List())
  }
  
  case class MultiSize(l: Map[Variable,Int], consts: Int) {
    def smaller(other: MultiSize) = other match {
      case MultiSize(l2, consts2) => 
        consts <= consts2 &&
        l.forall { case (v,c) => l2.contains(v) && c <= l2(v) }
    }
    
    def + (w: W) = w match {
      case Word(u) => MultiSize(l, consts+u.length)
      case vx @ Variable(x) => MultiSize(l.updated(Variable(x), l.getOrElse(vx,0) + 1), consts)
    }
    
    def getExpression: String = {
      if (l.isEmpty) consts.toString 
      else {
        val (v,i) = l.head
        val xs = l.tail
        "+ " + paren(xs.foldLeft(mult(v,i)) { case (expr, (w,j)) => "+ (" + expr + ") (" + mult(w,j) + ")" }) + " " + consts
      }
    }
  }
  
  var freshid = 0
  def fresh() = {
    freshid += 1
    Variable("F" + freshid)
  }
  
  abstract class W {
    def isWord: Boolean
    def isVariable: Boolean
  }
  case class Variable(v: String) extends W {
//     override def toString = "X" + v
    override def toString = v
    
    def isWord = false
    def isVariable = true
//     def compare(s2: String) = v compare s2
  }
  case class Word(s: String) extends W {
    override def toString = "'" + s + "'"
    
    def isWord = true
    def isVariable = false
  }
  
  object W {
    def apply(s: String): W = {
      if (s.isEmpty) throw new Exception("EMPTY")
      else if (s(0) == 'X') {
        val t: String = s.drop(1)
        Variable(s.drop(1))
      }
      else if (s(0) == '\'' && s(s.length-1) == '\'') Word(s.replace("'",""))
      else
        throw new Exception("Could not parse: " + s)
    }
  
  }
  
  
  
  implicit def ordering[A <: W]: Ordering[W] = Ordering.by(_.toString)
  
  case class Concatenation(vars: List[W]) {
  
  
    def renameVars(f: String => String) = {
      Concatenation(vars.map {
        case Word(u) => Word(u)
        case Variable(s) => Variable(f(s))
      })
    }
  
    
    def modify(i: Int, e: Equation): Concatenation = {
      val Equation(a,b) = e
      (take(i) ++ b ++ drop(i + a.length)).mergeWords
    }
    
    def modify(s: Set[Int], e: Equation): Concatenation = {
      s.toList.sorted.reverse.foldLeft(this)((d,i) => d.modify(i,e))
    }
    
    
    def oldsubstitutions(a: Concatenation, b: Concatenation): Set[Concatenation] = {
      val indices = this.sliding(a.size).
        zipWithIndex.
        filter { case (l,_) => l == a }.
        map { _._2 }.toSet
      
      indices.toSet.subsets.toSet.map((s: Set[Int]) => this.modify(s,Equation(a,b)))
    }
    
    
    def substitutions(a: Concatenation, b: Concatenation): Set[Concatenation] = {
      val na = a.length
      val nb = b.length
      val avars = a.vars
      val bvars = b.vars
      
//       println("a length: " + na)
      
      def loop(vs: List[W], r: Int): Set[List[W]] = {
//         println(vs,r)
//         println("===")
//         println(vs.take(na))
//         println(a)
//         println(vs.take(na) == a)
        vs match {
          case _ if (r < na) => Set(vs)
          case x :: xs if (vs.take(na) == avars) => 
//             println("HI")
            loop(xs, r-1).map { x :: _ } ++
            loop(vs.drop(na), r - na).map { bvars ::: _ }
          case x :: xs => loop(xs, r-1).map { x :: _ }
        }
      }
      
      
      val res = loop(vars, vars.length).map { (l: List[W]) => Concatenation(l) }
//       val old = oldsubstitutions(a,b)
//       if  (res != old) {
//         println("res")
//         println(res.toList.map(_.toString).sorted.sortBy(_.length).mkString("\n"))
//         println("old")
//         println(old.toList.map(_.toString).sorted.sortBy(_.length).mkString("\n"))
//         
//         println("\nArguments")
//         println(this)
//         println(a,b)
//         
//         throw new Exception("stop")
//       }
      res
    }
    
    def substitutions(e: Equation): Set[Concatenation] = {
      substitutions(e.a, e.b) ++ substitutions(e.b, e.a)
    }
  
    import Concatenation._
    
    def substitutions(s: Set[Equation]): Set[Concatenation] = {
      if (cache.contains((this,s))) cache((this,s))
      else {
        val res = s.flatMap(e => substitutions(e))
        if (cache.size < maxCacheSize) {
          cache = cache.updated((this,s), res)
        }
        res
      }
    }
  
    def apply(i: Int) = vars(i)

    def <->(c: Concatenation) = Equation(this,c)

    def ++(c: Concatenation) = Concatenation(vars ++ c.vars)

    def take(i: Int) = Concatenation(vars.take(i))
    def drop(i: Int) = Concatenation(vars.drop(i))

    def collectLetters: Set[Char] = vars.flatMap {
      case Word(w) => w.toList.toSet
      case Variable(v) => Set[Char]()
    }.toSet

    def isEmpty = vars.isEmpty

    def head = vars.head
    def tail = vars.tail

    def length = vars.length
    def size = vars.length
    def sorted = vars.sorted

    def reverse = Concatenation(vars.reverse.map {
      case Variable(v) => Variable(v)
      case Word(w) => Word(w.reverse)
    })

  
    def changeAll(e: Equation): Concatenation = {
      val Equation(a,b) = e
      val va = a.vars
      val vb = b.vars
      val na = va.length
      
      def loop(l: List[W]): List[W] = {
        if (l.length < na) l
        else if (l.take(na) == va) vb ::: loop(l.drop(na))
        else l.head :: loop(l.tail)
      }
      
      Concatenation(loop(vars)).mergeWords
    }
  
    def collectVars: List[Variable] = vars.flatMap {
      case Variable(v) => Some(Variable(v))
      case Word(_) => None
    }

    def substitute(v: Variable, c: Concatenation) = Concatenation(vars.flatMap {
      case w if (v == w) => c.vars
      case w => List(w)
    })

    def erase(v: Variable): Concatenation = Concatenation(vars.filterNot(_ == v))

    def containsWord(): Boolean = { 
    //       println("lol")
    //       println("vars: " + vars)
      vars.exists { (w: W) => 
    //       println("w: " + w)
        w match {
          case Word(_) => 
      //           println("word")
            true 
          case _ => 
      //           println("not word")
            false 
        }
      }
    }

    def msize: MultiSize = vars match {
      case Nil => MultiSize(Map(), 0)
      case x :: xs => xs.msize + x
    }
   
    
    def erase(w: W): Concatenation = w match {
      case Variable(v) => erase(Variable(v))
      case _ => this
    }
    
    def mergeWords() = {
      def loop(l: List[W]): List[W] = l match {
        case Nil => l
        case List(x) => l
        case Word(x) :: Word(y) :: xs => loop(Word(x + y) :: xs)
        case x :: xs => x :: loop(xs)
      }
      Concatenation(loop(vars))
    }
    
    def sliding(i: Int) = vars.sliding(i).map(Concatenation(_))
    
    override def toString() = vars.mkString(" ")
  }
  
  object Concatenation {
  
    def apply(): Concatenation = Concatenation(List())
    
    def apply(s: String): Concatenation = {
//       println("Parsing concatenation: " + s)
//       println(s.split(" ").toList)
      Concatenation(s.split(" ").toList.filterNot(_.isEmpty).map(W(_)))
    }  
    
    var cache = Map[(Concatenation, Set[Equation]), Set[Concatenation]]()
    val maxCacheSize = 10000000
  }
  
  implicit def wToConcatenation(v: W): Concatenation = Concatenation(List(v))
//   implicit def variablesToConcatenation(vars: List[W]): Concatenation = Concatenation(vars)
  implicit def wToConcatenation(vars: List[W]): Concatenation = Concatenation(vars)
  
  
  
  case class Equation(a: Concatenation, b: Concatenation) {
    def trivial = a == b
  
    def reverse = a.reverse <-> b.reverse
    def erase(v: Variable) = a.erase(v) <-> b.erase(v)
  
    def apply(i: Int): Concatenation = {
      if (i == 0) a
      else if (i == 1) b
      else throw new Exception("An equation has only 2 sides")
    }
    
    def substitutions(e: Equation): Set[Equation] = {
      
      val asub = a.substitutions(e)
      val bsub = b.substitutions(e)
      
      asub.flatMap(i => 
        bsub.map(j =>
          Equation(i,j)
        )
      )
    }
    
    import Equation._
  
    def substitutions(s: Set[Equation]): Set[Equation] = {
  //     println("subst: " + e)
  
      if (eqcache.contains((this,s))) eqcache((this,s))
      else {
        val asub = a.substitutions(s)
        val bsub = b.substitutions(s)
        val res = 
          asub.flatMap(i => 
            bsub.map(j =>
              Equation(i,j)
            )
          )
        if (eqcache.size < eqmaxCacheSize) {
          eqcache = eqcache.updated((this,s), res)
        }
        res
      }
  
      
      
      
    }
    
    
    def changeAll(e: Equation) = a.changeAll(e) <-> b.changeAll(e)
    
    def findPrefixEquations: Set[(Concatenation, Concatenation)] = {
      val na = a.size
      val nb = b.size
      
      (0 to na).toSet.flatMap { (i: Int) =>
        (0 to nb).toSet.flatMap { (j: Int) =>
          val x = a.take(i)
          val y = b.take(j)
          val mx = x.msize
          val my = y.msize
          if (mx smaller my) {
            Some((x,y))
          } else if (my smaller mx) {
            Some((y,x))
          } else {
            None
          }
        }
      }
    }
    
    def exploreArithmeticPrefix(f: Formula): Set[Equation] = {
    
      val na = a.size
      val nb = b.size
      
      val n = (na + 1)*(nb + 1)
      var k = 0
      
      (0 to na).toSet.flatMap { (i: Int) =>
        (0 to nb).toSet.flatMap { (j: Int) =>
          k += 1
          if (k % 10 == 0)
            println("Arithmetic Prefix Progress: " + k + " / " + n)
          val x = a.take(i)
          val y = b.take(j)
          val mx = x.msize
          val my = y.msize
          
          val z = a.drop(i)
          val t = b.drop(j)
          val mz = z.msize
          val mt = t.msize
          
//           if (x.size == 3 && y.size == 3) {
//           
//             println("x = " + x)
//             println("y = " + y)
//             
//             println(mx)
//             println(my)
//             
//             println((f + NonEquality(mx, my)).isSatisfiable)
//           
//           }
          
          if (!(f + NonEquality(mx, my)).isSatisfiable || !(f + NonEquality(mz, mt)).isSatisfiable)
            Set(Equation(x,y), Equation(z,t))
          else 
            Set[Equation]()
        }
      }
    }
    
    
  
    def explorePrefixes: Set[Equation] = {
      
//       println("Exploring prefixes of " + this)
      @tailrec
      def loop(pa: List[W], pb: List[W], a: List[W], b: List[W], acc: Set[Equation]): Set[Equation] = {
        (a, b) match {
          case (Nil, Nil) => 
            acc
          case (x::xs,y::ys) =>
            if (pa.msize == pb.msize || a.msize == b.msize) {
  //             println(pa.sorted,pb.sorted,a.sorted,b.sorted,acc)
              loop(pa :+ x, pb :+ y, xs, ys, acc ++ Set(pa <-> pb,a <-> b).filter(!_.trivial))
            }
            else
              loop(pa :+ x, pb :+ y, xs, ys, acc)
          
          case _ => acc
  //         case _ => throw new Exception("Equation(a,b) with a.length != b.length")
        }
      }
      
      
//       val Equation(a,b) = e
      val res =
        loop(List(), List(), a.vars, b.vars ,Set()) ++
        loop(List(), List(), a.reverse.vars, b.reverse.vars, Set()).map { e => e.reverse }
      
      if (res.isEmpty) Set(this)
      else res
//       println("Prefixes of " + this)
//       println(res)
      
//       res
    }
    
    
    def substitute(v: Variable, c: Concatenation) = a.substitute(v,c) <-> b.substitute(v,c)
//     def findFirstLetter: Map[Variable,Char] = (a.head, b.head) match {
//       case (Word(w), Variable(v)) => Map(Variable(v) -> w.head)
//       case (Variable(v), Word(w)) => Map(Variable(w) -> w.head)
//       case _ => Map()
//     }
  
    def simplify = {
      def loop(al: List[W], bl: List[W]): (List[W], List[W]) = (al,bl) match {
        case (x::xs, y::ys) if (x == y) => loop(xs,ys)
        case (Word(x)::xs, Word(y)::ys) if (x.startsWith(y)) =>
          loop(Word(x.drop(y.length)) :: xs, ys)
        case (Word(x)::xs, Word(y)::ys) if (y.startsWith(x)) =>
          loop(xs, Word(y.drop(x.length)) :: ys)
        case _ => (al,bl)
      }
      val (c,d) = loop(a.vars, b.vars)
      val (e,f) = loop(c.reverse, d.reverse)
//       println(c,d,e,f)
      e.reverse <-> f.reverse
    }
    
    def findHeadConstant(c: Concatenation, d: Concatenation): List[(Variable, Concatenation)] = {
      if (c.length > 0 && d.length > 0 && c(0).isWord && d(0).isVariable) {
        val vx@Variable(x) = d(0)
        List((vx, c(0) ++ vx))
      }
      else
        List()
    }
    
    def findConstants: List[(Variable, Concatenation)] = {
      findHeadConstant(a,b) ::: findHeadConstant(a.reverse, b.reverse).map { case (v,c) => (v,c.reverse) } :::
      findHeadConstant(b,a) ::: findHeadConstant(b.reverse, a.reverse).map { case (v,c) => (v,c.reverse) }
    }
    
    
    def goodConstants = {
      def loop(al: List[W], bl: List[W]): Boolean = (al,bl) match {
        case (Word(x) :: xs, Word(y) :: ys) => x.startsWith(y) || y.startsWith(x)
        case (Word(x) :: xs, Nil) => false
        case (Nil, Word(y) :: ys) => false
        case (_,_) => true
      }
      loop(a.vars, b.vars) &&
      loop(a.vars.reverse, b.vars.reverse)
    }
    
    def isContradictory = {
      (a.isEmpty && b.containsWord) ||
      (b.isEmpty && a.containsWord) ||
      !goodConstants
    }
    
    
    def findEmpty: List[Variable] = {
      val ma = a.msize
      val mb = b.msize
      
      if (ma smaller mb) 
        mb.l.filter { case (v,i) => !ma.l.contains(v) || mb.l(v) > ma.l(v) }.keys.toList
      else if (mb smaller ma)
        ma.l.filter { case (v,i) => !mb.l.contains(v) || ma.l(v) > mb.l(v) }.keys.toList
      else
        List()
//       if (a.isEmpty) b.collectVars 
//       else if (b.isEmpty) a.collectVars
//       else List()
    }

    def mergeWords = a.mergeWords <-> b.mergeWords
  
    override def equals(o: Any) = o match {
      case Equation(c,d) => (a == c && b == d) || (a == d && b == c)
      case _ => false
    }
    
    override def hashCode = a.hashCode * b.hashCode
    
    override def toString() = a + " = " + b
  }
  
  object Equation {
  
    def apply(s: String): Equation = {
      val l = s.split("=")
      val a = l(0)
      val b = l(1)
      
      Concatenation(a) <-> Concatenation(b)
    }
    
    
    
    var eqcache = Map[(Equation, Set[Equation]), Set[Equation]]()
    val eqmaxCacheSize = 10000000
    
  }
  
  object Problem {
    def apply(s: String): Problem = Problem(s.split("\n").filterNot(_.trim.isEmpty).map(Equation(_)).toSet, Description())
  }
  
  case class Problem(eqs: Set[Equation], desc: Description) {
    
    def collectVars = eqs.flatMap { case Equation(a,b) => a.collectVars ++ b.collectVars }
    def collectLetters = eqs.flatMap { case Equation(a,b) => a.collectLetters ++ b.collectLetters }
    
    def erase(v: Variable): Problem = Problem(eqs.map(_.erase(v)), desc).removeTrivial
    def erase(vs: Set[Variable]): Problem = vs.foldLeft(this)((p,v) => p.erase(v))
    def erase(vs: List[Variable]): Problem = vs.foldLeft(this)((p,v) => p.erase(v))

    def removeTrivial = Problem(eqs.filterNot(_.trivial), desc)
    
    def findEmpty = eqs.flatMap(_.findEmpty)
    
    def isContradictory = eqs.exists(_.isContradictory) || divergent || badsizes
    
    def simplify = Problem(eqs.map(_.simplify), desc)
    
    def mergeWords = Problem(eqs.map(_.mergeWords), desc)
    
    def reverse = Problem(eqs.map(_.reverse), desc)
    
    def substitute(v: Variable, c: Concatenation) = Problem(eqs.map(_.substitute(v,c)), desc)
    
    
    def badsizes = {
      val b = getFormula.isSatisfiable
      if (b) false
      else {
        println("Those sizes do not make sense.")
        true
      }
    }
    
    def cleanup: Problem = {
      val q = mergeWords.simplify.removeTrivial.removeDefinitions.explorePrefixes.removeconcat
      
      val vs = q.findEmpty
      
      val res = q.erase(vs)
      
      if (res == this) res
      else res.cleanup
    }
    
    def findConstants: List[Problem] = {
      val constants = eqs.toList.flatMap(_.findConstants)
      
      if (constants.exists { case (v,c) => 
        constants.exists { case (v2,c2) =>
          v == v2 && c != c2
        }
      }) {
        println("Found contradiction in the constants")
        println(constants.toMap.mkString("\n"))
        List()
      }
      else {
      
        def loop(l: List[(Variable,Concatenation)]): List[Problem] = l match {
          case Nil => List(this)
          case (v,c) :: xs =>
            loop(xs).flatMap { pb =>
              List(
                pb.changeAll(v,Concatenation()).addDesc(v + " is empty"),
                pb.changeAll(v,c).addDesc(v + " <- " + c)
              )
            }
        }
        
        loop(constants.distinct)
      }
    }
    

    def divergent = {
      println("Looking for a divergence in " + eqs.size + " equations.")
    
      val prefixeseqs = eqs.map { eq => (eq, eq.findPrefixEquations) }
      
//       println("prefixes")
//       println(prefixeseqs)
      
      prefixeseqs.exists { case (eqa, pas) =>
        pas.exists { case (pa1,pa2) =>
//           if (pa1 == Concatenation("XF29 '1'") && pa2 == Concatenation("XE XF29 '1'")) 
//             println("wow")
          val na = pa1.length
          na > 0 && (
          pa1(na-1) match {
            case Word(u) =>
              
//               if (pa1 == Concatenation("XF29 '1'") && pa2 == Concatenation("XE XF29 '1'")) 
//                 println(u)
              prefixeseqs.exists { case (eqb, pbs) =>
                pbs.exists { case (pb1,pb2) =>
                  val nb = pb1.length
                  
                  
                  pa2 == pb2 &&
                  na == nb && 
                  pa1.take(na-1) == pb1.take(nb-1) && 
                  pb1(nb-1).isWord &&
                  pb1(nb-1)(0) != pa1(na-1)(0)
                }
              }
            case Variable(_) => false
          }
          )
        }
      }
    }
  
    def removeconcat: Problem = {
      println("Removing equations which are concatenation of other equations (" + eqs.size + " equations).")
      
      var res = eqs
      for (Equation(a,b) <- eqs) {
//         println("Considering Equation")
//         println(Equation(a,b))
        
        for (i <- 1 to a.length-2) {
          for (j <- 1 to b.length-2) {
            val a1 = a.take(i)
            val a2 = a.drop(i)
            val b1 = b.take(j)
            val b2 = b.drop(j)
            
            if (eqs.contains(a1 <-> b1) && eqs.contains(a2 <-> b2)) {
//               println(a1,a2,b1,b2)
              res = res - Equation(a,b)
            }
          }
        }
      }
      
      Problem(res, desc)
    }
  
  
    def substitutions: Problem = {
  //     if (s.contains(target))
  //       throw new Exception("FOUND IT")
//       println(eqs.map(_.toString).toList.sorted.sortBy(_.size).mkString("\n"))
      
      val n = eqs.size
      println(n + " equations to substitute.")
      var i = 0
      Problem(eqs.flatMap { e => 
        i += 1
        println("Progress: " + i + " / " + n)
        e.substitutions(eqs)
      }, desc)
    }
      
    
    def ++(others: Set[Equation]) = Problem(eqs ++ others, desc)
    def -(other: Equation) = Problem(eqs - other, desc)
    def +(other: Equation) = Problem(eqs + other, desc)
    
    def changeAll(e: Equation): Problem = Problem(eqs.map(_.changeAll(e)), desc)
    def changeAll(a: Concatenation, b: Concatenation): Problem = Problem(eqs.map(_.changeAll(Equation(a,b))), desc)
    
    def existDefinitions: Boolean = {
      eqs.exists { case Equation(a,b) => (a.size == 1 && a(0).isVariable) || (b.size == 1 && b(0).isVariable) }
    }
    
    def removeDefinitions: Problem = {
//       println("REMOVING DEFINITIONS")
//       println(this)
      eqs.find { case Equation(a,b) => (a.size == 1 && a(0).isVariable) || (b.size == 1 && b(0).isVariable) } match {
        case None => this
        case Some(Equation(Concatenation(Variable(x) :: Nil),b)) =>
          val e = Equation(Concatenation(Variable(x) :: Nil),b)
          (this - e).changeAll(Equation(Variable(x),b)).removeDefinitions
        case Some(Equation(a,Concatenation(Variable(x) :: Nil))) =>
          val e = Equation(a,Concatenation(Variable(x) :: Nil))
          (this - e).changeAll(Equation(Variable(x),a)).removeDefinitions
        case _ => throw new Exception("Please")
      }
    }
  
  
    def removesubst: Problem = {
      println("Removing equations that can be obtained by substitution from other equations (" + eqs.size + " equations).")
      eqs.find { case Equation(c,d) =>
        fixpoint[Set[Concatenation]](Set(c), _.flatMap(_.substitutions(eqs - Equation(c,d)))).contains(d)
      } match {
        case None => this 
        case Some(e) => this - e
      }
    }
    
    
    def closeSubstitutions: Option[Problem] = {
      val res = substitutions
      
      println("Cache size: " + Concatenation.cache.size)
      println("Equations Cache size: " + Equation.eqcache.size)
      
      if (res == this) Some(res)
      else if (res.isContradictory) None
      else res.closeSubstitutions
    }
    
    def closeSubstPrefix: Problem = {
      val res = substitutions.explorePrefixes
      
      println("Cache size: " + Concatenation.cache.size)
      println("Equations Cache size: " + Equation.eqcache.size)
      
      if (res == this) res
      else res.closeSubstPrefix
    }
    
    def findCoreEquations: Problem = {
      val res = cleanup.removesubst
      
      if (res == this) res
      else res.findCoreEquations
    }
  
    def getFormula: Formula = {
      Formula(eqs.toList.map { case Equation(a,b) => Equality(a.msize, b.msize) })
    }
  
    def changeDescription(d2: Description) = Problem(eqs,d2)
    def addDesc(s: String) = Problem(eqs, desc add s)
    
    def exploreArithmeticPrefix: Problem = {
      val f = getFormula
      
      Problem(eqs.flatMap { e => 
        val pp = e.exploreArithmeticPrefix(f)
        if (pp.isEmpty) Set(e)
        else pp
      }, desc)
    }
    
    def exploreArithmeticPrefix(e: Equation): Problem = {
      val f = getFormula
      
      val pp = e.exploreArithmeticPrefix(f)
//       println("pp")
//       println(pp.mkString("\n"))
      
      this ++ pp
//       if (pp.isEmpty) this
//       else (this -
//       Problem(eqs.flatMap { e => 
//         val pp = e.exploreArithmeticPrefix(f)
//         if (pp.isEmpty) Set(e)
//         else pp
//       })
    }
  
    def explorePrefixes: Problem = {
//       println(eq.map(_.toString).toList.sorted.sortBy(_.size).mkString("\n"))
//       println(eqs.size + " equations to explore")
  //     if (s.contains(target))
  //       throw new Exception("FOUND IT")
      val res = Problem(eqs.flatMap(_.explorePrefixes), desc)
  //     println(res.show)
  //     println("mememetrimming")
//       val r = simplify(trim(res))
//       val r = 
  //     println("mims")
      res
    }
    
    override def toString: String = {
      eqs.map(_.toString).toList.sorted.sortBy(_.size).mkString("\n")
    }
    
//     def nonSingCleanup: Option[Problem] = {
//       val q1 = mergeWords 
//       val q2 = q1.simplify
//       val vs = q2.findEmpty
//       
//       println("merged")
//       println(q1)
//       
//       println("simplified")
//       println(q2)
//       
//       
//       if (!vs.isEmpty || q2.isContradictory)
//         None
//       else
//         Some(q2)
//     }
  }
}
