import java.io.FileWriter

import scala.sys.process._
import scala.language.postfixOps

import Constraints._

object Arithmetic {
  
  
  
  abstract class Atom {
    def collectVariables: Set[Variable]
    
    def getAssertion: String
  }
  
  def paren(s: String) = if (s.contains(" ")) "(" + s + ")" else s
  
  case class Equality(m1: MultiSize, m2: MultiSize) extends Atom {
    def collectVariables: Set[Variable] = m1.l.keySet ++ m2.l.keySet
    
    def getAssertion = {
      "(assert (= " + paren(m1.getExpression) + " " + paren(m2.getExpression) + "))"
    }
  }
  
  case class NonEquality(m1: MultiSize, m2: MultiSize) extends Atom {
    def collectVariables: Set[Variable] = m1.l.keySet ++ m2.l.keySet
    
    def getAssertion = {
      "(assert (not (= " + paren(m1.getExpression) + " " + paren(m2.getExpression) + ")))"
    }
  }
  

  val sat = """sat""".r.unanchored
  val unsat = """unsat""".r.unanchored
  
  case class Formula(atoms: List[Atom]) {
    
    def collectVariables: Set[Variable] = atoms.flatMap(_.collectVariables).toSet
  
    def +(a: Atom) = Formula(a :: atoms)
  
    def isSatisfiable = {
    
      val fw = new FileWriter("temp.smt2")
      
      for (v <- collectVariables)
        fw.write("(declare-const " + v + " Int)\n")
        
      for (a <- atoms) {
        fw.write(a.getAssertion + "\n")
      }
      
      fw.write("(check-sat)")
      fw.close
      
      val output = ("z3 temp.smt2") !!
      
//       println("Z3 output: " + output)
      
      output match {
        case unsat() => false
        case sat() => true
        case _ =>
          throw new Exception("z3 failed with output: " + output)
      }
    }
  }
  
  
  
}
